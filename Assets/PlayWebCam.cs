﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class PlayWebCam : MonoBehaviour {

	public RawImage rawimage;
    WebCamTexture webcamTexture;

    void Start () 
	{
		webcamTexture = new WebCamTexture();
		rawimage.texture = webcamTexture;
		rawimage.material.mainTexture = webcamTexture;
		webcamTexture.Play();
	}

    void Update()
    {
        //This is to take the picture, save it and stop capturing the camera image.
        if (Input.GetKey(KeyCode.B))
        {
            SaveImage();
            webcamTexture.Stop();
           
        }
    }
    void SaveImage()
    {
        //Create a Texture2D with the size of the rendered image on the screen.
        Texture2D texture = new Texture2D(rawimage.texture.width, rawimage.texture.height, TextureFormat.ARGB32, false);

        //Save the image to the Texture2D
        texture.SetPixels(webcamTexture.GetPixels());
        texture.Apply();

        //Encode it as a PNG.
        byte[] bytes = texture.EncodeToPNG();

        //Save it in a file.
        File.WriteAllBytes(Application.dataPath + "/images/testimg.png", bytes);
    }




}
